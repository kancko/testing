﻿ALTER DATABASE mysqldbtest DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;

DROP TABLE IF EXISTS answers;
DROP TABLE IF EXISTS questions;
DROP TABLE IF EXISTS tests;
DROP TABLE IF EXISTS subjects;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS users_tests;
DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL
);

CREATE TABLE users (
  id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  role ENUM('ADMIN','STUDENT') NOT NULL,
  group_id BIGINT(20),
  enabled TINYINT(1) NOT NULL DEFAULT 1,
  FOREIGN KEY(group_id) REFERENCES `groups`(id)
);


CREATE TABLE subjects (
  id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL
);

CREATE TABLE tests (
  id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  subject_id BIGINT(20) NOT NULL,
  complexity ENUM('LOW','MEDIUM','HARD') NOT NULL,
  duration_minutes INT(11) NOT NULL,
  questions_size INT(11) NOT NULL DEFAULT 0,
  FOREIGN KEY(subject_id) REFERENCES subjects(id)
);

CREATE TABLE users_tests (
	id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	user_id BIGINT NOT NULL,
	test_id BIGINT NOT NULL,
	mark DOUBLE NOT NULL,
	FOREIGN KEY(user_id) REFERENCES users(id),
	FOREIGN KEY(test_id) REFERENCES tests(id)
);

CREATE TABLE questions (
  id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  description TEXT,
  test_id BIGINT(20) NOT NULL,
  FOREIGN KEY(test_id) REFERENCES tests(id)
);

CREATE TABLE answers (
  id BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  is_correct TINYINT(1) NOT NULL,
  question_id BIGINT(20) NOT NULL,
  FOREIGN KEY(question_id) REFERENCES questions(id)
);

insert into users(name,password,email,role) values('admin','21232F297A57A5A743894A0E4A801FC3','admin@mail.com','ADMIN');
insert into subjects(id, name) values(1, 'математика'), (2, 'литература'), (3, 'физика');
insert into tests(id, name, subject_id, complexity, duration_minutes)
    values(1, 'математика 2-й семестр 1 класс', 1, 'MEDIUM', 10),
          (2, 'литература 1-й семестр 3 класс', 2, 'LOW', 10),
          (3, 'физика 2-й семестр 9 класс', 3, 'HARD', 10);
insert into questions(name, description, test_id) values ('Посчитать сумму', 'Сколько будет 12+3?', 1),
                                                               ('Посчитать сумму', 'Сколько будет 7+9?', 1),
                                                               ('Посчитать разность', 'Сколько будет 23-3?', 1);
update tests set questions_size = 3 where id = 1;