function countDown() {
    var timer = document.getElementById("timer");
    var seconds = timer.innerHTML * 60;
    var timer = setInterval(function() {
        if (seconds > 0) {
            seconds --;
            var m = seconds/60 ^ 0,
                s = seconds-m*60,
                time = (m<10?"0"+m:m)+":"+(s<10?"0"+s:s);
            document.getElementById("timer").innerHTML = time;
        } else {
            document.getElementById("testForm").submit();

        }
    }, 1000);
}