<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="title" value="Create Test form"/>
<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div id="login">
    <a href="/admin/admin-home" id="back">
        <i class="fas fa-arrow-circle-left"></i>
    </a>
    <div class="span_sign">
        <span><fmt:message key="createTest"/></span>
    </div>
    <form action="/admin/create-test" method="post">

        <fieldset class="clearfix">
            <p><span class="fontawesome-file-alt"></span><input type="text" name="name" placeholder="<fmt:message key="testname"/>" required>
            </p>
            <p><span class="fontawesome-book"></span><input type="text" name="subject" placeholder="<fmt:message key="subject"/>" required>
            </p>
            <p><span class="fontawesome-cog"></span><select name="complexity" size="1" id="complexityTestCreate">
                <option value="LOW" selected>Low</option>
                <option value="MEDIUM">Medium</option>
                <option value="HARD">Hard</option>
            </select></p>
            <p><span class="fontawesome-time"></span><input type="number" name="duration_minutes" min="1" max="60"
                                                            value="10" placeholder="<fmt:message key="duration"/>" required></p>

            <p><input type="submit" name="submit" value="<fmt:message key="save"/>"></p>
        </fieldset>
    </form>
</div>
</body>
</html>