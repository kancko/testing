<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="title" value="Test Pass"/>
<%@ include file="/WEB-INF/jspf/header.jspf" %>

<div id="testPass">
    <p><span id="timer" style="color: #4af; font-size: 150%;
                        font-weight: bold;">${test.durationMinutes}</span></p>
    <p>${test.name}</p>
    <form method="post" id="testForm" action="/student/test-pass">
        <input type="hidden" name="id" value="${test.id}">
        <c:forEach var="question" items="${test.questions}">
            <div id="questionTestPass">
                <input type="hidden" name="questionId" value="${question.id}">
                <p>${question.name}</p>
                <p>${question.description}</p>
                <br>
                <c:forEach var="answer" items="${question.answers}">
                    <div id="answerTestPass">
                        <input type="hidden" name="id" value="${answer.id}">
                        <p>${answer.name}</p>
                        <div class="check-material">
                            <input id="label-${answer.id}" type="checkbox" name="answers"
                                   value="${question.id},${answer.id}">
                            <label for="label-${answer.id}"></label>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </c:forEach>
        <input type="submit" value="<fmt:message key="complete"/>">
    </form>
</div>
</body>
</html>