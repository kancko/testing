<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="title" value="Admin Panel"/>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<%@ taglib prefix="la" tagdir="/WEB-INF/tags"%>

<la:lang />

<div id="wrapper">
    <div id="user_info">
        <div id="title_user">
            <div id="icon"><i class="fas fa-user" style="padding-top: 8px;"></i></div>
            <div id="welcome_user"><fmt:message key="admin"/> ${user.name}</div>
            <a href="/logout"><i class="fas fa-sign-out-alt"></i></a>
        </div>
        <hr>
        <span><fmt:message key="email"/>: ${user.email}</span><br>
        <span><fmt:message key="acctype"/>: ${user.role}</span><br>
    </div>
    <div id="tests" class="tableInfo">
        <a href="/admin/create-test" id="add"><i class="fas fa-plus-circle"></i></a>
        <div style="display:block; text-align:center;">
            <span><fmt:message key="tests"/></span>
        </div>
        <form id="subSearch">
            <c:if test="${param.size != null}">
                <input type="hidden" name="size" value="${param.size}">
            </c:if>
            <c:if test="${param.order != null}">
                <input type="hidden" name="order" value="${param.order}">
            </c:if>
            <input type="text" name="subjectName" value="${param.subjectName}" placeholder="<fmt:message key="subname"/>">
        </form>
        <form id="pageSize">
            <c:if test="${param.subjectName != null}">
                <input type="hidden" name="subjectName" value="${param.subjectName}">
            </c:if>
            <c:if test="${param.order != null}">
                <input type="hidden" name="order" value="${param.order}">
            </c:if>
            <input type="number" name="size" value="${param.size}" min="1" placeholder="<fmt:message key="size"/>">
        </form>
        <table>

            <c:url value="/admin/admin-home" var="sortName">
                <c:set var="ascDesc" value="DESC"/>
                <c:choose>
                    <c:when test="${param.order == 't.name,ASC'}">
                        <c:set var="ascDesc" value="DESC"/>
                    </c:when>
                    <c:when test="${param.order == 't.name,DESC'}">
                        <c:set var="ascDesc" value="ASC"/>
                    </c:when>
                </c:choose>
                <c:param name="order" value="t.name,${ascDesc}"/>
                <c:if test="${param.subjectName != null}">
                    <c:param name="subjectName" value="${param.subjectName}"/>
                </c:if>
                <c:if test="${param.size != null}">
                    <c:param name="size" value="${param.size}"/>
                </c:if>
            </c:url>

            <c:url value="/admin/admin-home" var="sortSubject">
                <c:set var="ascDesc" value="ASC"/>
                <c:choose>
                    <c:when test="${param.order == 's.name,ASC'}">
                        <c:set var="ascDesc" value="DESC"/>
                    </c:when>
                    <c:when test="${param.order == 's.name,DESC'}">
                        <c:set var="ascDesc" value="ASC"/>
                    </c:when>
                </c:choose>
                <c:param name="order" value="s.name,${ascDesc}"/>
                <c:if test="${param.subjectName != null}">
                    <c:param name="subjectName" value="${param.subjectName}"/>
                </c:if>
                <c:if test="${param.size != null}">
                    <c:param name="size" value="${param.size}"/>
                </c:if>
            </c:url>

            <c:url value="/admin/admin-home" var="sortComplexity">
                <c:set var="ascDesc" value="ASC"/>
                <c:choose>
                    <c:when test="${param.order == 't.complexity,ASC'}">
                        <c:set var="ascDesc" value="DESC"/>
                    </c:when>
                    <c:when test="${param.order == 't.complexity,DESC'}">
                        <c:set var="ascDesc" value="ASC"/>
                    </c:when>
                </c:choose>
                <c:param name="order" value="t.complexity,${ascDesc}"/>
                <c:if test="${param.subjectName != null}">
                    <c:param name="subjectName" value="${param.subjectName}"/>
                </c:if>
                <c:if test="${param.size != null}">
                    <c:param name="size" value="${param.size}"/>
                </c:if>
            </c:url>

            <c:url value="/admin/admin-home" var="sortDuration">
                <c:set var="ascDesc" value="ASC"/>
                <c:choose>
                    <c:when test="${param.order == 't.duration_minutes,ASC'}">
                        <c:set var="ascDesc" value="DESC"/>
                    </c:when>
                    <c:when test="${param.order == 't.duration_minutes,DESC'}">
                        <c:set var="ascDesc" value="ASC"/>
                    </c:when>
                </c:choose>
                <c:param name="order" value="t.duration_minutes,${ascDesc}"/>
                <c:if test="${param.subjectName != null}">
                    <c:param name="subjectName" value="${param.subjectName}"/>
                </c:if>
                <c:if test="${param.size != null}">
                    <c:param name="size" value="${param.size}"/>
                </c:if>
            </c:url>

            <c:url value="/admin/admin-home" var="sortAmount">
                <c:set var="ascDesc" value="ASC"/>
                <c:choose>
                    <c:when test="${param.order == 't.questions_size,ASC'}">
                        <c:set var="ascDesc" value="DESC"/>
                    </c:when>
                    <c:when test="${param.order == 't.questions_size,DESC'}">
                        <c:set var="ascDesc" value="ASC"/>
                    </c:when>
                </c:choose>
                <c:param name="order" value="t.questions_size,${ascDesc}"/>
                <c:if test="${param.subjectName != null}">
                    <c:param name="subjectName" value="${param.subjectName}"/>
                </c:if>
                <c:if test="${param.size != null}">
                    <c:param name="size" value="${param.size}"/>
                </c:if>
            </c:url>
            <tr>
                <th><a href='<c:out value="${sortName}" />'><fmt:message key="testname"/></a></th>
                <th><a href='<c:out value="${sortSubject}" />'><fmt:message key="subject"/></a></th>
                <th><a href='<c:out value="${sortComplexity}" />'><fmt:message key="complexity"/></a></th>
                <th><a href='<c:out value="${sortDuration}" />'><fmt:message key="duration"/></a></th>
                <th><a href='<c:out value="${sortAmount}" />'><fmt:message key="amount"/></a></th>
            </tr>
            <c:forEach var="test" items="${tests}">
                <tr onclick="window.location.href='/admin/edit-test?id=${test.id}'; return false" id="link">
                    <td>${test.name}</td>
                    <td>${test.subject.name}</td>
                    <td>${test.complexity}</td>
                    <td>${test.durationMinutes}</td>
                    <td>${test.questionsSize}</td>
                </tr>
            </c:forEach>
        </table>

        <div id="pagination">
            <ex:co testSize = "${testsSize}" size="${size}" var="count"/>
            <c:if test="${size != null}">
                <c:forEach var="i" begin="1" end="${count}">
                    <c:url value="/admin/admin-home" var="paginationUrl">
                        <c:if test="${size*i-size != 0}">
                            <c:param name="offset" value="${size*i-size}"/>
                        </c:if>
                        <c:if test="${size != null}">
                            <c:param name="size" value="${size}"/>
                        </c:if>
                        <c:if test="${param.subjectName != null}">
                            <c:param name="subjectName" value="${param.subjectName}"/>
                        </c:if>
                    </c:url>
                    <a href='<c:out value="${paginationUrl}" />'>${i}</a>
                </c:forEach>
            </c:if>
        </div>

        <hr>
        <div style="display:block; text-align:center; ">
            <span><fmt:message key="students"/></span>
        </div>
        <table style="width: -webkit-fill-available;">
            <tr>
                <th><fmt:message key="studname"/></th>
                <th><fmt:message key="email"/></th>
                <th><fmt:message key="group"/></th>
                <th><fmt:message key="block"/></th>
            </tr>
            <c:forEach var="user" items="${students}">
                <tr>
                    <td>${user.name}</td>
                    <td>${user.email}</td>
                    <td>${user.group.name}</td>
                    <td>
                        <form method="post" action="/admin/user-update-enabled" id="lockForm">
                            <fmt:message key="enabled" var="enabled"/>
                            <fmt:message key="disabled" var="disabled"/>
                            <input name="userId" type="hidden" value="${user.id}">
                            <input name="enabled" type="hidden" value="${!user.enabled}">
                            <input type="submit" value="${user.enabled ? disabled : enabled}">
                        </form>
                    </td>
                </tr>
            </c:forEach>
        </table>

        <hr>
        <div style="display:block; text-align:center; ">
            <span><fmt:message key="passtest"/></span>
        </div>
        <table style="width: -webkit-fill-available;">
            <tr>
                <th><fmt:message key="studname"/></th>
                <th><fmt:message key="email"/></th>
                <th><fmt:message key="group"/></th>
                <th><fmt:message key="mark"/></th>
                <th><fmt:message key="testname"/></th>
                <th><fmt:message key="subject"/></th>
            </tr>
            <c:forEach var="userTest" items="${usersTests}">
                <tr>
                    <td>${userTest.user.name}</td>
                    <td>${userTest.user.email}</td>
                    <td>${userTest.user.group.name}</td>
                    <td><fmt:formatNumber type="percent" maxIntegerDigits="3" minFractionDigits="1" value="${userTest.mark}"/></td>
                    <td>${userTest.test.name}</td>
                    <td>${userTest.test.subject.name}</td>
                </tr>
            </c:forEach>
        </table>

    </div>
</div>

</body>
</html>
