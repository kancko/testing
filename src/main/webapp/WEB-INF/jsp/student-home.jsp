<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="title" value="User Homepage"/>
<%@ include file="/WEB-INF/jspf/header.jspf" %>
<%@ taglib prefix="la" tagdir="/WEB-INF/tags"%>

<la:lang />

<div id="wrapper">
    <div id="user_info">
        <div id="title_user">
            <div id="icon"><i class="fas fa-user" style="padding-top: 8px;"></i></div>
            <div id="welcome_user"><fmt:message key="welcome"/>, ${user.name}</div>
            <a href="/logout"><i class="fas fa-sign-out-alt"></i></a>
        </div>
        <hr>
        <span><fmt:message key="email"/>: ${user.email}</span><br>
        <span><fmt:message key="acctype"/>: ${user.role}</span><br>
        <span><fmt:message key="group"/>: ${user.group.name}</span>
    </div>
    <div id="tests" class="tableInfo">
        <div style="display:block; text-align:center; padding-top:10px;">
            <span><fmt:message key="tests"/></span>
        </div>
        <form id="subSearch">
            <c:if test="${param.size != null}">
                <input type="hidden" name="size" value="${param.size}">
            </c:if>
            <c:if test="${param.order != null}">
                <input type="hidden" name="order" value="${param.order}">
            </c:if>
            <input type="text" name="subjectName" value="${param.subjectName}" placeholder="<fmt:message key="subname"/>">
        </form>
        <form id="pageSize">
            <c:if test="${param.subjectName != null}">
                <input type="hidden" name="subjectName" value="${param.subjectName}">
            </c:if>
            <c:if test="${param.order != null}">
                <input type="hidden" name="order" value="${param.order}">
            </c:if>
            <input type="number" name="size" value="${param.size}" min="1" placeholder="<fmt:message key="size"/>">
        </form>
        <table>

            <c:url value="/student/student-home" var="sortName">
                <c:set var="ascDesc" value="DESC"/>
                <c:choose>
                    <c:when test="${param.order == 't.name,ASC'}">
                        <c:set var="ascDesc" value="DESC"/>
                    </c:when>
                    <c:when test="${param.order == 't.name,DESC'}">
                        <c:set var="ascDesc" value="ASC"/>
                    </c:when>
                </c:choose>
                <c:param name="order" value="t.name,${ascDesc}"/>
                <c:if test="${param.subjectName != null}">
                    <c:param name="subjectName" value="${param.subjectName}"/>
                </c:if>
                <c:if test="${param.size != null}">
                    <c:param name="size" value="${param.size}"/>
                </c:if>
            </c:url>

            <c:url value="/student/student-home" var="sortSubject">
                <c:set var="ascDesc" value="ASC"/>
                <c:choose>
                    <c:when test="${param.order == 's.name,ASC'}">
                        <c:set var="ascDesc" value="DESC"/>
                    </c:when>
                    <c:when test="${param.order == 's.name,DESC'}">
                        <c:set var="ascDesc" value="ASC"/>
                    </c:when>
                </c:choose>
                <c:param name="order" value="s.name,${ascDesc}"/>
                <c:if test="${param.subjectName != null}">
                    <c:param name="subjectName" value="${param.subjectName}"/>
                </c:if>
                <c:if test="${param.size != null}">
                    <c:param name="size" value="${param.size}"/>
                </c:if>
            </c:url>

            <c:url value="/student/student-home" var="sortComplexity">
                <c:set var="ascDesc" value="ASC"/>
                <c:choose>
                    <c:when test="${param.order == 't.complexity,ASC'}">
                        <c:set var="ascDesc" value="DESC"/>
                    </c:when>
                    <c:when test="${param.order == 't.complexity,DESC'}">
                        <c:set var="ascDesc" value="ASC"/>
                    </c:when>
                </c:choose>
                <c:param name="order" value="t.complexity,${ascDesc}"/>
                <c:if test="${param.subjectName != null}">
                    <c:param name="subjectName" value="${param.subjectName}"/>
                </c:if>
                <c:if test="${param.size != null}">
                    <c:param name="size" value="${param.size}"/>
                </c:if>
            </c:url>

            <c:url value="/student/student-home" var="sortDuration">
                <c:set var="ascDesc" value="ASC"/>
                <c:choose>
                    <c:when test="${param.order == 't.duration_minutes,ASC'}">
                        <c:set var="ascDesc" value="DESC"/>
                    </c:when>
                    <c:when test="${param.order == 't.duration_minutes,DESC'}">
                        <c:set var="ascDesc" value="ASC"/>
                    </c:when>
                </c:choose>
                <c:param name="order" value="t.duration_minutes,${ascDesc}"/>
                <c:if test="${param.subjectName != null}">
                    <c:param name="subjectName" value="${param.subjectName}"/>
                </c:if>
                <c:if test="${param.size != null}">
                    <c:param name="size" value="${param.size}"/>
                </c:if>
            </c:url>

            <c:url value="/student/student-home" var="sortAmount">
                <c:set var="ascDesc" value="ASC"/>
                <c:choose>
                    <c:when test="${param.order == 't.questions_size,ASC'}">
                        <c:set var="ascDesc" value="DESC"/>
                    </c:when>
                    <c:when test="${param.order == 't.questions_size,DESC'}">
                        <c:set var="ascDesc" value="ASC"/>
                    </c:when>
                </c:choose>
                <c:param name="order" value="t.questions_size,${ascDesc}"/>
                <c:if test="${param.subjectName != null}">
                    <c:param name="subjectName" value="${param.subjectName}"/>
                </c:if>
                <c:if test="${param.size != null}">
                    <c:param name="size" value="${param.size}"/>
                </c:if>
            </c:url>
            <tr>
                <th><a href='<c:out value="${sortName}" />'><fmt:message key="testname"/></a></th>
                <th><a href='<c:out value="${sortSubject}" />'><fmt:message key="subject"/></a></th>
                <th><a href='<c:out value="${sortComplexity}" />'><fmt:message key="complexity"/></a></th>
                <th><a href='<c:out value="${sortDuration}" />'><fmt:message key="duration"/></a></th>
                <th><a href='<c:out value="${sortAmount}" />'><fmt:message key="amount"/></a></th>
            </tr>
            <c:forEach var="test" items="${tests}">
                <tr onclick="window.location.href='/student/test-pass?id=${test.id}'; return false" id="link">
                    <td>${test.name}</td>
                    <td>${test.subject.name}</td>
                    <td>${test.complexity}</td>
                    <td>${test.durationMinutes}</td>
                    <td>${test.questionsSize}</td>
                </tr>
            </c:forEach>
        </table>

        <div id="pagination">
            <ex:co testSize = "${testsSize}" size="${size}" var="count"/>
            <c:if test="${size != null}">
                <c:forEach var="i" begin="1" end="${count}">
                    <c:url value="/student/student-home" var="paginationUrl">
                        <c:if test="${size*i-size != 0}">
                            <c:param name="offset" value="${size*i-size}"/>
                        </c:if>
                        <c:if test="${size != null}">
                            <c:param name="size" value="${size}"/>
                        </c:if>
                        <c:if test="${param.subjectName != null}">
                            <c:param name="subjectName" value="${param.subjectName}"/>
                        </c:if>
                    </c:url>
                    <a href='<c:out value="${paginationUrl}" />'>${i}</a>
                </c:forEach>
            </c:if>
        </div>

        <hr>
        <div style="display:block; text-align:center; padding-top:10px;">
            <span><fmt:message key="passtest"/></span>
        </div>
        <table style="width: -webkit-fill-available;">
            <tr>
                <th><fmt:message key="testname"/></th>
                <th><fmt:message key="subject"/></th>
                <th><fmt:message key="mark"/></th>
            </tr>
            <c:forEach var="test" items="${userTests}">
                <tr>
                    <td>${test.test.name}</td>
                    <td>${test.test.subject.name}</td>
                    <td><fmt:formatNumber type="percent" maxIntegerDigits="3" minFractionDigits="1" value="${test.mark}"/></td>
                </tr>
            </c:forEach>
        </table>
    </div>
</div>
</body>
</html>
