package ua.nure.sidorov.test.service;

import ua.nure.sidorov.test.entity.Answer;
import ua.nure.sidorov.test.entity.Question;
import ua.nure.sidorov.test.entity.Test;
import ua.nure.sidorov.test.entity.UserTest;

import java.util.List;
import java.util.Map;

public interface TestService {

    Test getById(Long id);

    List<Test> getAll(String subjectName, String orderBy, int offset, int size);

    Long getCount(String subjectName);

    Long save(Test test);

    void update(Test test);

    void delete(Long id);

    void incrementTestQuestionSize(Long id);

    void decrementTestQuestionSize(Long id);

    List<UserTest> getTestResultsByUserId(Long id);

    List<UserTest> getAllUsersTests();

    Long saveTestResultByUserIdAndTestId(UserTest userTest, Map<Question,List<Answer>> questionMap);

    Long selectUserTestByUserAndTestId(UserTest userTest);

    void updateUserTestById(UserTest userTest, Map<Question, List<Answer>> qestionsMap);

    void setQuestionService(QuestionService questionService);

    boolean getUsersTestsByTestId(Long id);
}
