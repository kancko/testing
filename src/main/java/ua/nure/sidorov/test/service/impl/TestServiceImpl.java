package ua.nure.sidorov.test.service.impl;

import ua.nure.sidorov.test.entity.*;
import ua.nure.sidorov.test.exception.ValidationEnum;
import ua.nure.sidorov.test.exception.ValidationException;
import ua.nure.sidorov.test.exception.ValidationExceptionBuilder;
import ua.nure.sidorov.test.repository.TestRepository;
import ua.nure.sidorov.test.service.AnswerService;
import ua.nure.sidorov.test.service.QuestionService;
import ua.nure.sidorov.test.service.SubjectService;
import ua.nure.sidorov.test.service.TestService;
import ua.nure.sidorov.test.transaction.TransactionManager;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public class TestServiceImpl implements TestService {

    private TransactionManager transactionManager;
    private TestRepository testRepository;
    private SubjectService subjectService;
    private AnswerService answerService;
    private QuestionService questionService;

    public TestServiceImpl(TransactionManager transactionManager, TestRepository testRepository,
                           SubjectService subjectService, AnswerService answerService) {
        this.transactionManager = transactionManager;
        this.testRepository = testRepository;
        this.subjectService = subjectService;
        this.answerService = answerService;
    }

    @Override
    public Test getById(Long id) {
        return transactionManager.execute(c -> testRepository.getById(c, id));
    }

    @Override
    public List<Test> getAll(String subjectName, String orderBy, int offset, int size) {
        return transactionManager.execute(
                c -> testRepository.getAll(c, subjectName, orderBy, offset, size));
    }

    @Override
    public Long getCount(String subjectName) {
        return transactionManager.execute(c -> testRepository.getCount(c, subjectName));
    }

    @Override
    public Long save(Test test) {
        return transactionManager.execute(c -> {
            saveOrGetSubject(test);
            return testRepository.save(c, test);
        });
    }

    @Override
    public void update(Test test) {
        transactionManager.execute(c -> {
            saveOrGetSubject(test);
            testRepository.update(c, test);
            return null;
        });
    }

    private void saveOrGetSubject(Test test) {
        Entity subject = subjectService.getByName(test.getSubject().getName());
        if (Objects.isNull(subject)) {
            Long subjectId = subjectService.save(test.getSubject());
            test.getSubject().setId(subjectId);
        } else {
            test.setSubject(subject);
        }
    }

    @Override
    public void delete(Long id) {
        transactionManager.execute(c -> {
            testRepository.delete(c, id);
            return null;
        });
    }

    @Override
    public void incrementTestQuestionSize(Long id) {
        transactionManager.execute(c -> {
            testRepository.incrementTestQuestionSize(c, id);
            return null;
        });
    }

    @Override
    public void decrementTestQuestionSize(Long id) {
        transactionManager.execute(c -> {
            testRepository.decrementTestQuestionSize(c, id);
            return null;
        });
    }

    @Override
    public List<UserTest> getTestResultsByUserId(Long id) {
        return transactionManager.execute(c -> testRepository.getTestResultsByUserId(c, id));
    }

    @Override
    public List<UserTest> getAllUsersTests() {
        return transactionManager.execute(c->testRepository.getAllUsersTests(c));
    }

    @Override
    public Long saveTestResultByUserIdAndTestId(UserTest userTest, Map<Question, List<Answer>> qestionsMap) {
        List<Question> questionList = questionService.getAllByTestId(userTest.getTest().getId());
        for (Question question : questionList) {
            question.setAnswers(answerService.getCorrectByQuestionId(question.getId()));
        }

        int questionsSize = questionList.size();
        int numOfCorrectAnswers = getNumberOfCorrectAnswers(qestionsMap, questionList);
        Double mark = ((double) numOfCorrectAnswers) / questionsSize;
        userTest.setMark(mark.isNaN()?0:mark);
        return transactionManager.execute(c -> testRepository.saveTestResultByUserIdAndTestId(c, userTest));
    }

    @Override
    public Long selectUserTestByUserAndTestId(UserTest userTest) {
        return transactionManager.execute(c-> testRepository.
                selectUserTestByUserAndTestId(c, userTest.getUser().getId(), userTest.getTest().getId()));
    }

    @Override
    public void updateUserTestById(UserTest userTest, Map<Question, List<Answer>> qestionsMap) {
        List<Question> questionList = questionService.getAllByTestId(userTest.getTest().getId());
        for (Question question : questionList) {
            question.setAnswers(answerService.getCorrectByQuestionId(question.getId()));
        }

        int questionsSize = questionList.size();
        int numOfCorrectAnswers = getNumberOfCorrectAnswers(qestionsMap, questionList);
        Double mark = ((double) numOfCorrectAnswers) / questionsSize;
        userTest.setMark(mark.isNaN()?0:mark);

        transactionManager.execute(c-> {
            testRepository.updateUserTestById(c, userTest);
            return null;
        });
    }

    @Override
    public boolean getUsersTestsByTestId(Long id) {
        return transactionManager.execute(c -> testRepository.getUsersTestsByTestId(c, id));
    }

    public void setQuestionService(QuestionService questionService) {
        this.questionService = questionService;
    }

    private static int getNumberOfCorrectAnswers(Map<Question, List<Answer>> questionsMap, List<Question> questionList) {
        int count = 0;
        for (Map.Entry<Question, List<Answer>> questionEntry : questionsMap.entrySet()) {
            for (Question question : questionList) {
                if (questionEntry.getKey().getId().equals(question.getId()) &&
                        questionEntry.getValue().size() == question.getAnswers().size() &&
                        questionEntry.getValue().containsAll(question.getAnswers())) {
                    count++;
                    break;
                }
            }
        }
        return count;
    }

}
