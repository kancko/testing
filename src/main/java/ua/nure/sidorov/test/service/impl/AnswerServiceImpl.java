package ua.nure.sidorov.test.service.impl;

import ua.nure.sidorov.test.entity.Answer;
import ua.nure.sidorov.test.entity.Question;
import ua.nure.sidorov.test.repository.AnswerRepository;
import ua.nure.sidorov.test.service.AnswerService;
import ua.nure.sidorov.test.transaction.TransactionManager;

import java.util.List;
import java.util.Objects;

public class AnswerServiceImpl implements AnswerService {

    private TransactionManager transactionManager;
    private AnswerRepository answerRepository;

    public AnswerServiceImpl(TransactionManager transactionManager, AnswerRepository answerRepository) {
        this.transactionManager = transactionManager;
        this.answerRepository = answerRepository;
    }

    @Override
    public List<Answer> getAll() {
        return transactionManager.execute(c -> answerRepository.getAll(c));
    }

    @Override
    public List<Answer> getByQuestionId(Long id) {
        return transactionManager.execute(c->answerRepository.getByQuestionId(c,id));
    }

    @Override
    public List<Answer> getCorrectByQuestionId(Long id) {
        return transactionManager.execute(c->answerRepository.getCorrectByQuestionId(c, id));
    }

    @Override
    public Long save(Answer answer) {
        return transactionManager.execute(c -> answerRepository.save(c, answer));
    }

    @Override
    public void delete(Long id) {
        transactionManager.execute(c -> {
            answerRepository.delete(c, id);
            return null;
        });
    }

    @Override
    public void update(Answer answer) {
        transactionManager.execute(c -> {
            answerRepository.update(c, answer);
            return null;
        });
    }

    @Override
    public void fetchAnswers(List<Question> questions) {
        if (Objects.nonNull(questions) && !questions.isEmpty()) {
            transactionManager.execute(c -> {
                for (Question question : questions) {
                    question.setAnswers(answerRepository.getByQuestionId(c, question.getId()));
                }
                return null;
            });
        }
    }
}
