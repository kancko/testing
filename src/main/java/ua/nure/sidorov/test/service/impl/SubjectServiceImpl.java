package ua.nure.sidorov.test.service.impl;

import ua.nure.sidorov.test.entity.Entity;
import ua.nure.sidorov.test.repository.SubjectRepository;
import ua.nure.sidorov.test.service.SubjectService;
import ua.nure.sidorov.test.transaction.TransactionManager;

public class SubjectServiceImpl implements SubjectService {

    private TransactionManager transactionManager;
    private SubjectRepository subjectRepository;

    public SubjectServiceImpl(TransactionManager transactionManager, SubjectRepository subjectRepository) {
        this.transactionManager = transactionManager;
        this.subjectRepository = subjectRepository;
    }

    @Override
    public Entity getByName(String name) {
        return transactionManager.execute(c -> subjectRepository.getByName(c, name));
    }

    @Override
    public boolean existByName(String name) {
        return transactionManager.execute(c -> subjectRepository.existByName(c, name));
    }

    @Override
    public Long save(Entity subject) {
        return transactionManager.execute(c -> subjectRepository.save(c, subject));
    }
}
