package ua.nure.sidorov.test.service.impl;

import ua.nure.sidorov.test.entity.Entity;
import ua.nure.sidorov.test.entity.Student;
import ua.nure.sidorov.test.entity.User;
import ua.nure.sidorov.test.exception.ValidationEnum;
import ua.nure.sidorov.test.exception.ValidationException;
import ua.nure.sidorov.test.repository.UserRepository;
import ua.nure.sidorov.test.service.GroupService;
import ua.nure.sidorov.test.service.UserService;
import ua.nure.sidorov.test.transaction.TransactionManager;

import java.util.List;
import java.util.Objects;

public class UserServiceImpl implements UserService {

    private TransactionManager transactionManager;
    private GroupService groupService;
    private UserRepository userRepository;

    public UserServiceImpl(TransactionManager transactionManager, UserRepository userRepository, GroupService groupService) {
        this.transactionManager = transactionManager;
        this.userRepository = userRepository;
        this.groupService = groupService;
    }

    @Override
    public User getById(Long id) {
        return transactionManager.execute(c -> userRepository.getById(c, id));
    }

    @Override
    public List<User> getAll() {
        return transactionManager.execute(c -> userRepository.getAll(c));
    }

    @Override
    public List<Student> getAllStudents() {
        return transactionManager.execute(c -> userRepository.getAllStudents(c));
    }

    @Override
    public User getUserByEmailAndPassword(String email, String password) {
        return transactionManager.execute(c -> userRepository.getUserByEmailAndPassword(c, email, password));
    }

    @Override
    public Long save(User user) {
        return transactionManager.execute(c ->
        {
            serGroup(user);
            return userRepository.save(c, user);
        });
    }

    private void serGroup(User user) {
        if (user instanceof Student) {
            Student student = ((Student) user);
            Entity group = groupService.getByName(student.getGroup().getName());
            if (Objects.isNull(group)) {
                Long id = groupService.save(student.getGroup());
                student.getGroup().setId(id);
            } else {
                student.setGroup(group);
            }
        }
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public void update(User user) {
        if (existsByIdAndEmail(user.getId(), user.getEmail())) {
            ValidationException.builder().put("emailError", ValidationEnum.EMAIL_EXISTS);
        } else {
            transactionManager.execute(c -> {
                userRepository.update(c, user);
                return null;
            });
        }
    }

    private boolean existsByIdAndEmail(Long id, String email) {
        return transactionManager.execute(c -> userRepository.existsByIdAndEmail(c, id, email));
    }


    @Override
    public boolean existsByEmail(String email) {
        return transactionManager.execute(c -> userRepository.existsByEmail(c, email));
    }

    @Override
    public boolean isEnabledById(Long id) {
        return transactionManager.execute(c -> userRepository.isEnabledById(c, id));
    }

    @Override
    public void updateEnabled(Long id, boolean b) {
        transactionManager.execute(c -> {
            userRepository.updateEnabled(c, id, b);
            return null;
        });
    }
}
