package ua.nure.sidorov.test.service;

import ua.nure.sidorov.test.entity.Entity;

public interface GroupService {
    Entity getByName(String name);

    boolean existByName(String name);

    Long save(Entity group);
}
