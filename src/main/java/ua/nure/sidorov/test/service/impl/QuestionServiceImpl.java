package ua.nure.sidorov.test.service.impl;

import ua.nure.sidorov.test.entity.Question;
import ua.nure.sidorov.test.repository.QuestionRepository;
import ua.nure.sidorov.test.service.QuestionService;
import ua.nure.sidorov.test.service.TestService;
import ua.nure.sidorov.test.transaction.TransactionManager;

import java.util.List;

public class QuestionServiceImpl implements QuestionService {

    private TransactionManager transactionManager;
    private QuestionRepository questionRepository;
    private TestService testService;

    public QuestionServiceImpl(TransactionManager transactionManager, QuestionRepository questionRepository, TestService testService) {
        this.transactionManager = transactionManager;
        this.questionRepository = questionRepository;
        this.testService = testService;
    }

    @Override
    public List<Question> getAll(Long testId) {
        return transactionManager.execute(c -> questionRepository.getAll(c, testId));
    }

    @Override
    public Question getById(Long id) {
        return transactionManager.execute(c -> questionRepository.getById(c, id));
    }

    @Override
    public Long save(Question question) {
        return transactionManager.execute(c -> {
            testService.incrementTestQuestionSize(question.getTest().getId());
            return questionRepository.save(c, question);
        });
    }

    @Override
    public void delete(Question question) {
        transactionManager.execute(c -> {
            testService.decrementTestQuestionSize(question.getTest().getId());
            questionRepository.delete(c, question.getId());
            return null;
        });
    }

    @Override
    public void update(Question question) {
        transactionManager.execute(c -> {
            questionRepository.update(c, question);
            return null;
        });
    }

    @Override
    public List<Question> getAllByTestId(Long id) {
        return transactionManager.execute(c -> questionRepository.getByTestId(c, id));
    }

}
