package ua.nure.sidorov.test.service;

import ua.nure.sidorov.test.entity.Answer;
import ua.nure.sidorov.test.entity.Question;

import java.util.List;

public interface AnswerService {

    List<Answer> getAll();

    List<Answer> getByQuestionId(Long id);

    Long save(Answer answer);

    void delete(Long id);

    void update(Answer answer);

    void fetchAnswers(List<Question> questions);

    List<Answer> getCorrectByQuestionId(Long id);
}
