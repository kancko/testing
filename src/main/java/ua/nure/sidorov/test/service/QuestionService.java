package ua.nure.sidorov.test.service;

import ua.nure.sidorov.test.entity.Question;

import java.util.List;

public interface QuestionService {

    Question getById(Long id);

    List<Question> getAll(Long testId);

    Long save(Question question);

    void delete(Question question);

    void update(Question question);

    List<Question> getAllByTestId(Long id);

}
