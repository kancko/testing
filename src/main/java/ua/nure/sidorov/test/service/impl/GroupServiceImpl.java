package ua.nure.sidorov.test.service.impl;

import ua.nure.sidorov.test.entity.Entity;
import ua.nure.sidorov.test.repository.GroupRepository;
import ua.nure.sidorov.test.repository.UserRepository;
import ua.nure.sidorov.test.service.GroupService;
import ua.nure.sidorov.test.transaction.TransactionManager;

public class GroupServiceImpl implements GroupService {

    private TransactionManager transactionManager;
    private GroupRepository groupRepository;

    public GroupServiceImpl(TransactionManager transactionManager, GroupRepository groupRepository) {
        this.transactionManager = transactionManager;
        this.groupRepository = groupRepository;
    }

    @Override
    public Entity getByName(String name) {
        return transactionManager.execute(c -> groupRepository.getByName(c, name));
    }

    @Override
    public boolean existByName(String name) {
        return transactionManager.execute(c -> groupRepository.existByName(c, name));
    }

    @Override
    public Long save(Entity group) {
        return transactionManager.execute(c -> groupRepository.save(c, group));
    }
}
