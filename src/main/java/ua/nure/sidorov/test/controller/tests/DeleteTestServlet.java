package ua.nure.sidorov.test.controller.tests;

import ua.nure.sidorov.test.controller.common.ConverterUtils;
import ua.nure.sidorov.test.exception.ValidationEnum;
import ua.nure.sidorov.test.exception.ValidationException;
import ua.nure.sidorov.test.service.TestService;

import javax.servlet.ServletConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/admin/delete-test")
public class DeleteTestServlet extends HttpServlet {

    private TestService testService;

    @Override
    public void init(ServletConfig config) {
        testService = (TestService) config.getServletContext().getAttribute(TestService.class.toString());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws IOException {
        Long testId = ConverterUtils.getIdFromRequest(request);
        if (!testService.getUsersTestsByTestId(testId)) {
            testService.delete(testId);
            resp.sendRedirect("/admin/admin-home");
        } else {
            ValidationException.builder().put("deleteError", ValidationEnum.DELETE_ERROR).throwIfErrorExists();
        }
    }
}
