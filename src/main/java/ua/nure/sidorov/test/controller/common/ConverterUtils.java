package ua.nure.sidorov.test.controller.common;

import ua.nure.sidorov.test.entity.*;
import ua.nure.sidorov.test.exception.ValidationEnum;
import ua.nure.sidorov.test.exception.ValidationException;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

public class ConverterUtils {

    public static Test convertRequestToTest(HttpServletRequest request) {
        Entity subject = new Entity();
        subject.setName(request.getParameter("subject"));
        Test test = new Test();
        test.setId(getIdFromRequest(request));
        test.setName(request.getParameter("name"));
        test.setSubject(subject);
        test.setComplexity(Complexity.valueOf(request.getParameter("complexity")));
        test.setDurationMinutes(Integer.parseInt(request.getParameter("duration_minutes")));
        return test;
    }

    public static Question convertRequestToQuestion(HttpServletRequest request) {
        Question question = new Question();
        question.setId(getIdFromRequest(request));
        question.setName(request.getParameter("name"));
        question.setDescription(request.getParameter("description"));
        question.setTest(new Test(getTestIdFromRequest(request)));
        return question;
    }

    public static Student convertRequestToStudent(HttpServletRequest request) {
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String reRassword = request.getParameter("re_password");
        String groupName = request.getParameter("group");
        if (!password.equals(reRassword)) {
            ValidationException.builder().put("passwordError", ValidationEnum.PASSWORDS_DO_NOT_MATCH).throwIfErrorExists();
        }
        Student student = new Student();
        student.setName(name);
        student.setEmail(email);
        student.setPassword(PasswordEncoder.encode(password));
        student.setEnabled(true);
        student.setRole(Role.STUDENT);

        Entity group = new Entity();
        group.setName(groupName);
        student.setGroup(group);
        return student;
    }

    public static Answer convertRequestToAnswer(HttpServletRequest request) {
        Answer answer = new Answer();
        answer.setId(getIdFromRequest(request));
        answer.setName(request.getParameter("name"));
        answer.setCorrect(Boolean.parseBoolean(request.getParameter("isCorrect")));
        Question question = new Question(getQuestionIdFromRequest(request));
        question.setTest(new Test(getTestIdFromRequest(request)));
        answer.setQuestion(question);
        return answer;
    }

    public static Long getIdFromRequest(HttpServletRequest request) {
        if (Objects.nonNull(request.getParameter("id"))) {
            return Long.parseLong(request.getParameter("id"));
        } else {
            return null;
        }
    }

    private static Long getTestIdFromRequest(HttpServletRequest request) {
        if (Objects.nonNull(request.getParameter("testId"))) {
            return Long.parseLong(request.getParameter("testId"));
        } else {
            return null;
        }
    }

    private static Long getQuestionIdFromRequest(HttpServletRequest request) {
        if (Objects.nonNull(request.getParameter("questionId"))) {
            return Long.parseLong(request.getParameter("questionId"));
        } else {
            return null;
        }
    }

    public static Map<Question, List<Answer>> createRequestToQuestionMap(HttpServletRequest request) {
        String[] answers = request.getParameterValues("answers");
        if (Objects.isNull(answers) || answers.length == 0) {
            return Collections.emptyMap();
        }
        Map<Question, List<Answer>> questionsMap = new HashMap<>();
        List<Answer> answersList = new ArrayList<>();
        Answer answer;
        Question question;
        Long prevQuestionId = Long.parseLong(answers[0].split(",")[0]);
        for (String answerInList : answers) {
            Long questionId = Long.parseLong(answerInList.split(",")[0]);
            Long answerId = Long.parseLong(answerInList.split(",")[1]);
            answer = new Answer();
            answer.setId(answerId);
            question = new Question();
            question.setId(questionId);
            if (questionId.equals(prevQuestionId)) {
                answersList.add(answer);
            } else {
                answersList = new ArrayList<>();
                answersList.add(answer);
                prevQuestionId = questionId;
            }
            questionsMap.put(question, answersList);
        }
        return questionsMap;
    }

    public static UserTest convertRequestToUserTest(HttpServletRequest request) {
        UserTest userTest = new UserTest();
        userTest.setTest(new Test());
        userTest.getTest().setId(ConverterUtils.getIdFromRequest(request));
        userTest.setUser(new User());
        userTest.getUser().setId(((User) request.getSession().getAttribute("user")).getId());
        return userTest;
    }
}
