package ua.nure.sidorov.test.controller.auth;

import ua.nure.sidorov.test.controller.common.PasswordEncoder;
import ua.nure.sidorov.test.controller.common.JspConstants;
import ua.nure.sidorov.test.entity.Role;
import ua.nure.sidorov.test.entity.User;
import ua.nure.sidorov.test.exception.ValidationEnum;
import ua.nure.sidorov.test.exception.ValidationException;
import ua.nure.sidorov.test.service.UserService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;

@WebServlet(urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    private UserService userService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        userService = (UserService) config.getServletContext().getAttribute(UserService.class.toString());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher(JspConstants.USER_LOGIN_JSP).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String email = request.getParameter("email");
        String password = PasswordEncoder.encode(request.getParameter("password"));
        User user = userService.getUserByEmailAndPassword(email, password);
        if (Objects.nonNull(user)) {
            HttpSession session = request.getSession();
            session.setAttribute("user", user);
            if (Role.STUDENT.equals(user.getRole())) {
                response.sendRedirect("/student/student-home");
            } else {
                response.sendRedirect("/admin/admin-home");
            }
        } else {
            ValidationException.builder().put("loginError", ValidationEnum.BAD_CREDENTIAL).throwIfErrorExists();
        }
    }
}
