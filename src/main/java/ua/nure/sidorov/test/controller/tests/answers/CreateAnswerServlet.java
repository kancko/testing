package ua.nure.sidorov.test.controller.tests.answers;

import ua.nure.sidorov.test.controller.common.ConverterUtils;
import ua.nure.sidorov.test.entity.Answer;
import ua.nure.sidorov.test.service.AnswerService;

import javax.servlet.ServletConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/admin/create-answer")
public class CreateAnswerServlet extends HttpServlet {

    private AnswerService answerService;

    @Override
    public void init(ServletConfig config) {
        answerService = (AnswerService) config.getServletContext().getAttribute(AnswerService.class.toString());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Answer answer = ConverterUtils.convertRequestToAnswer(request);
        answerService.save(answer);
        response.sendRedirect("/admin/edit-test?id=" + answer.getQuestion().getTest().getId());
    }
}
