package ua.nure.sidorov.test.controller.tests;

import ua.nure.sidorov.test.controller.common.ConverterUtils;
import ua.nure.sidorov.test.controller.common.JspConstants;
import ua.nure.sidorov.test.entity.Test;
import ua.nure.sidorov.test.service.AnswerService;
import ua.nure.sidorov.test.service.QuestionService;
import ua.nure.sidorov.test.service.TestService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/admin/edit-test")
public class EditTestServlet extends HttpServlet {

    private TestService testService;
    private QuestionService questionService;
    private AnswerService answerService;

    @Override
    public void init(ServletConfig config) {
        testService = (TestService) config.getServletContext().getAttribute(TestService.class.toString());
        questionService = (QuestionService) config.getServletContext().getAttribute(QuestionService.class.toString());
        answerService = (AnswerService) config.getServletContext().getAttribute(AnswerService.class.toString());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Test test = testService.getById(ConverterUtils.getIdFromRequest(request));
        test.setQuestions(questionService.getAllByTestId(test.getId()));
        answerService.fetchAnswers(test.getQuestions());

        request.setAttribute("test", test);
        request.getRequestDispatcher(JspConstants.EDIT_TEST_JSP).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Test test = ConverterUtils.convertRequestToTest(req);
        testService.update(test);
        resp.sendRedirect("/admin/edit-test?id=" + test.getId());
    }
}
