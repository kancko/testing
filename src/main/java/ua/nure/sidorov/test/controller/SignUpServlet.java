package ua.nure.sidorov.test.controller;

import ua.nure.sidorov.test.controller.common.ConverterUtils;
import ua.nure.sidorov.test.controller.common.JspConstants;
import ua.nure.sidorov.test.entity.Student;
import ua.nure.sidorov.test.exception.ValidationEnum;
import ua.nure.sidorov.test.exception.ValidationException;
import ua.nure.sidorov.test.service.UserService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(urlPatterns = "/sign-up")
public class SignUpServlet extends HttpServlet {

    private UserService userService;

    @Override
    public void init(ServletConfig config) {
        userService = (UserService) config.getServletContext().getAttribute(UserService.class.toString());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher(JspConstants.SIGN_UP).forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

        Student student = ConverterUtils.convertRequestToStudent(request);
        if (userService.existsByEmail(student.getEmail())) {
            ValidationException.builder().put("emailError", ValidationEnum.EMAIL_EXISTS).throwIfErrorExists();
        }
        student.setId(userService.save(student));
        HttpSession session = request.getSession();
        session.setAttribute("user", student);
        response.sendRedirect("/student/student-home");

    }
}
