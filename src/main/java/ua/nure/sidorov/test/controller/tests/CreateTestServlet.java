package ua.nure.sidorov.test.controller.tests;

import ua.nure.sidorov.test.controller.common.ConverterUtils;
import ua.nure.sidorov.test.controller.common.JspConstants;
import ua.nure.sidorov.test.service.TestService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/admin/create-test")
public class CreateTestServlet extends HttpServlet {

    private TestService testService;

    @Override
    public void init(ServletConfig config) {
        testService = (TestService) config.getServletContext().getAttribute(TestService.class.toString());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher(JspConstants.CREATE_TEST_JSP).forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Long testId = testService.save(ConverterUtils.convertRequestToTest(request));
        response.sendRedirect("/admin/edit-test?id=" + testId);
    }
}
