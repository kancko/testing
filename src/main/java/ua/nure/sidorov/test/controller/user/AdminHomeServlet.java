package ua.nure.sidorov.test.controller.user;

import ua.nure.sidorov.test.controller.common.JspConstants;
import ua.nure.sidorov.test.controller.common.PaginationUtils;
import ua.nure.sidorov.test.entity.Student;
import ua.nure.sidorov.test.entity.Test;
import ua.nure.sidorov.test.entity.UserTest;
import ua.nure.sidorov.test.service.TestService;
import ua.nure.sidorov.test.service.UserService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/admin/admin-home")
public class AdminHomeServlet extends HttpServlet {

    private TestService testService;
    private UserService userService;

    @Override
    public void init(ServletConfig config) {
        testService = (TestService) config.getServletContext().getAttribute(TestService.class.toString());
        userService = (UserService) config.getServletContext().getAttribute(UserService.class.toString());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int offset = PaginationUtils.getOffset(request);
        int size = PaginationUtils.getSize(request);
        String subjectName = request.getParameter("subjectName");

        List<Test> testList = testService.getAll(subjectName, request.getParameter("order"), offset, size);
        List<UserTest> userTestList = testService.getAllUsersTests();
        List<Student> studentList = userService.getAllStudents();

        request.setAttribute("tests", testList);
        request.setAttribute("usersTests", userTestList);
        request.setAttribute("testsSize", testService.getCount(subjectName));
        request.setAttribute("students", studentList);

        request.getRequestDispatcher(JspConstants.HOMEPAGE_ADMIN_JSP).forward(request, response);
    }
}
