package ua.nure.sidorov.test.controller.common;

public class JspConstants {

    public static final String USER_LOGIN_JSP = "/WEB-INF/jsp/login.jsp";

    public static final String HOMEPAGE_JSP = "/WEB-INF/jsp/student-home.jsp";

    public static final String HOMEPAGE_ADMIN_JSP = "/WEB-INF/jsp/admin-home.jsp";

    public static final String SIGN_UP = "/WEB-INF/jsp/sign-up.jsp";

    public static final String CREATE_TEST_JSP = "/WEB-INF/jsp/create-test.jsp";

    public static final String EDIT_TEST_JSP = "/WEB-INF/jsp/edit-test.jsp";

    public static final String PASS_TEST_JSP = "/WEB-INF/jsp/test-pass.jsp";
}
