package ua.nure.sidorov.test.controller.user;

import ua.nure.sidorov.test.controller.common.JspConstants;
import ua.nure.sidorov.test.controller.common.PaginationUtils;
import ua.nure.sidorov.test.entity.Test;
import ua.nure.sidorov.test.entity.User;
import ua.nure.sidorov.test.entity.UserTest;
import ua.nure.sidorov.test.service.TestService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/student/student-home")
public class StudentHomeServlet extends HttpServlet {

    private TestService testService;

    @Override
    public void init(ServletConfig config) {
        testService = (TestService) config.getServletContext().getAttribute(TestService.class.toString());
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int offset = PaginationUtils.getOffset(request);
        int size = PaginationUtils.getSize(request);
        String subjectName = request.getParameter("subjectName");
        Long id = ((User) request.getSession().getAttribute("user")).getId();

        List<Test> testList = testService.getAll(request.getParameter("subjectName"), request.getParameter("order"), offset, size);
        List<UserTest> userTests = testService.getTestResultsByUserId(id);

        request.setAttribute("tests", testList);
        request.setAttribute("userTests", userTests);
        request.setAttribute("testsSize", testService.getCount(subjectName));

        request.getRequestDispatcher(JspConstants.HOMEPAGE_JSP).forward(request, response);
    }
}
