package ua.nure.sidorov.test.controller.tests.questions;

import ua.nure.sidorov.test.controller.common.ConverterUtils;
import ua.nure.sidorov.test.entity.Question;
import ua.nure.sidorov.test.service.QuestionService;

import javax.servlet.ServletConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/admin/delete-question")
public class DeleteQuestionServlet extends HttpServlet {

    private QuestionService questionService;

    @Override
    public void init(ServletConfig config) {
        questionService = (QuestionService) config.getServletContext().getAttribute(QuestionService.class.toString());
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Question question = ConverterUtils.convertRequestToQuestion(request);
        questionService.delete(question);
        response.sendRedirect("/admin/edit-test?id=" + question.getTest().getId());
    }
}
