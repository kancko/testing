package ua.nure.sidorov.test.tag;


import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class CountPagesTag extends SimpleTagSupport{

    private int testSize;
    private int size;
    private String var;

    public void setVar(String var) {
        this.var = var;
    }

    private int count;

    public void setSize(int size) {
        this.size = size;
    }

    public void setTestSize(int testSize) {
        this.testSize = testSize;
    }

    public void doTag() {
        count = testSize / size + ((testSize % size) > 0 ? 1 : 0);
        getJspContext().setAttribute(var, count);
    }
}
