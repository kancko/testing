package ua.nure.sidorov.test.config;

import org.apache.log4j.Logger;
import ua.nure.sidorov.test.repository.*;
import ua.nure.sidorov.test.repository.impl.*;
import ua.nure.sidorov.test.service.*;
import ua.nure.sidorov.test.service.impl.*;
import ua.nure.sidorov.test.transaction.TransactionManager;
import ua.nure.sidorov.test.transaction.impl.TransactionManagerImpl;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.sql.DataSource;

public class ApplicationContextListener implements ServletContextListener {

    private static final Logger LOGGER = Logger.getLogger(ApplicationContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        LOGGER.debug("context init start");
        ServletContext context = servletContextEvent.getServletContext();

        TransactionManager transactionManager = initTransactionManager();
        initServices(context, transactionManager);
        LOGGER.debug("context init finish");
    }

    private void initServices(ServletContext context, TransactionManager transactionManager) {
        UserRepository userRepository = new UserRepositoryImpl();
        GroupRepository groupRepository = new GroupRepositoryImpl();
        TestRepository testRepository = new TestRepositoryImpl();
        SubjectRepository subjectRepository = new SubjectRepositoryImpl();
        QuestionRepository questionRepository = new QuestionRepositoryImpl();
        AnswerRepository answerRepository = new AnswerRepositoryImpl();

        SubjectService subjectService = new SubjectServiceImpl(transactionManager, subjectRepository);
        GroupService groupService = new GroupServiceImpl(transactionManager, groupRepository);
        UserService userService = new UserServiceImpl(transactionManager, userRepository, groupService);
        AnswerService answerService = new AnswerServiceImpl(transactionManager, answerRepository);
        TestService testService = new TestServiceImpl(transactionManager, testRepository, subjectService, answerService);
        QuestionService questionService = new QuestionServiceImpl(transactionManager, questionRepository, testService);
        testService.setQuestionService(questionService);

        context.setAttribute(UserService.class.toString(), userService);
        context.setAttribute(TestService.class.toString(), testService);
        context.setAttribute(QuestionService.class.toString(), questionService);
        context.setAttribute(AnswerService.class.toString(), answerService);
    }

    private TransactionManager initTransactionManager() {
        try {
            DataSource dataSource = (DataSource) new InitialContext().lookup("java:comp/env/jdbc/test");
            return new TransactionManagerImpl(dataSource);
        } catch (NamingException e) {
            throw new RuntimeException("data source cant init");
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
