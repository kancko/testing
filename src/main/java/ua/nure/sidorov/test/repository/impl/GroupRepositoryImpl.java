package ua.nure.sidorov.test.repository.impl;

import ua.nure.sidorov.test.entity.Entity;
import ua.nure.sidorov.test.repository.GroupRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GroupRepositoryImpl implements GroupRepository {


    private static final String SELECT_GROUP_BY_NAME = "select * from mysqldbtest.groups where name = ?";
    private static final String EXIST_GROUP_BY_NAME = "select 1 from mysqldbtest.groups where name = ?";
    private static final String INSERT_GROUP = "insert into mysqldbtest.groups (name) values (?)";

    @Override
    public Entity getByName(Connection connection, String name) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_GROUP_BY_NAME);
        statement.setString(1, name);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next() ? convertResultSetToGroup(resultSet) : null;
    }

    @Override
    public boolean existByName(Connection connection, String name) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(EXIST_GROUP_BY_NAME);
        statement.setString(1, name);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next();
    }

    @Override
    public Long save(Connection connection, Entity group) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(INSERT_GROUP, PreparedStatement.RETURN_GENERATED_KEYS);
        setAttributeForSave(group, statement);
        statement.executeUpdate();
        return getGeneratedId(statement);
    }

    private void setAttributeForSave(Entity group, PreparedStatement statement) throws SQLException {
        statement.setString(1, group.getName());
    }

    private Long getGeneratedId(PreparedStatement statement) throws SQLException {
        ResultSet generatedKeys = statement.getGeneratedKeys();
        generatedKeys.next();
        return generatedKeys.getLong(1);
    }

    private Entity convertResultSetToGroup(ResultSet resultSet) throws SQLException {
        Entity group = new Entity();
        group.setId(resultSet.getLong("id"));
        group.setName(resultSet.getString("name"));
        return group;
    }
}
