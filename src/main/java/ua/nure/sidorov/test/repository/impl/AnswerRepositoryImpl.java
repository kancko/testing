package ua.nure.sidorov.test.repository.impl;

import ua.nure.sidorov.test.entity.Answer;
import ua.nure.sidorov.test.entity.Question;
import ua.nure.sidorov.test.repository.AnswerRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AnswerRepositoryImpl implements AnswerRepository {

    private static final String SELECT_ALL = "select * from answers";
    private static final String SELECT_BY_QUESTION_ID = "select * from answers where question_id = ?";
    private static final String INSERT_ANSWER = "insert into answers (name, is_correct, question_id) value(?,?,?)";
    private static final String DELETE_ANSWER = "delete from answers where id=?";
    private static final String UPDATE_ANSWER = "update answers set name=?, is_correct=? where id = ?";
    private static final String SELECT_CORRECT_BY_QUESTION_ID = "select * from answers where question_id = ? and is_correct = ?";

    @Override
    public List<Answer> getAll(Connection connection) throws SQLException {
        List<Answer> list = new ArrayList<>();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SELECT_ALL);
        while (resultSet.next()) {
            list.add(convertResultSetToUser(resultSet));
        }
        return list;
    }

    @Override
    public Long save(Connection connection, Answer answer) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(INSERT_ANSWER, PreparedStatement.RETURN_GENERATED_KEYS);
        setAttributeForSave(answer, statement);
        statement.executeUpdate();
        return getGeneratedId(statement);
    }

    @Override
    public void delete(Connection connection, Long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE_ANSWER);
        statement.setLong(1, id);
        statement.executeUpdate();
    }

    @Override
    public void update(Connection connection, Answer answer) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE_ANSWER);
        setAttributeForUpdate(answer, statement);
        statement.executeUpdate();
    }

    @Override
    public List<Answer> getByQuestionId(Connection connection, Long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_BY_QUESTION_ID);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();
        List<Answer> answers = new ArrayList<>();
        while (resultSet.next()) {
            answers.add(convertResultSetToUser(resultSet));
        }
        return answers;
    }

    @Override
    public List<Answer> getCorrectByQuestionId(Connection connection, Long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_CORRECT_BY_QUESTION_ID);
        statement.setLong(1, id);
        statement.setBoolean(2, true);
        ResultSet resultSet = statement.executeQuery();
        List<Answer> answers = new ArrayList<>();
        while (resultSet.next()) {
            answers.add(convertResultSetToUser(resultSet));
        }
        return answers;
    }

    private Long getGeneratedId(PreparedStatement statement) throws SQLException {
        ResultSet generatedKeys = statement.getGeneratedKeys();
        generatedKeys.next();
        return generatedKeys.getLong(1);
    }

    private void setAttributeForSave(Answer answer, PreparedStatement statement) throws SQLException {
        int count = 1;
        statement.setString(count++, answer.getName());
        statement.setBoolean(count++, answer.isCorrect());
        statement.setLong(count,answer.getQuestion().getId());
    }

    private void setAttributeForUpdate(Answer answer, PreparedStatement statement) throws SQLException {
        int count = 1;
        statement.setString(count++, answer.getName());
        statement.setBoolean(count++, answer.isCorrect());
        statement.setLong(count, answer.getId());
    }



    private Answer convertResultSetToUser(ResultSet resultSet) throws SQLException {
        Answer answer = new Answer();
        answer.setId(resultSet.getLong("id"));
        answer.setName(resultSet.getString("name"));
        answer.setCorrect(resultSet.getBoolean("is_correct"));
        return answer;
    }
}
