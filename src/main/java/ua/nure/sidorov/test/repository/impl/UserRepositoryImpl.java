package ua.nure.sidorov.test.repository.impl;

import ua.nure.sidorov.test.entity.Entity;
import ua.nure.sidorov.test.entity.Role;
import ua.nure.sidorov.test.entity.Student;
import ua.nure.sidorov.test.entity.User;
import ua.nure.sidorov.test.repository.UserRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryImpl implements UserRepository {

    private static final String SELECT_USER_BY_ID = "select * from users where id = ?";
    private static final String SELECT_ALL = "select * from users";
    private static final String SELECT_USER_BY_EMAIL_AND_PASSWORD = "select * from users left join `groups` on users.group_id=`groups`.id where email = ? and password = ?";
    private static final String INSERT_USER = "insert into users (name, password, email, role, group_id) values (?,?,?, ?, ?)";
    private static final String UPDATE_USER = "update users set name=?, password=?, email=? where id=?";
    private static final String DELETE_USER = "delete from users where id=?";
    private static final String EXISTS_USER_BY_ID_AND_EMAIL = "select 1 from users where id = ? and email = ?";
    private static final String EXISTS_USER_BY_EMAIL = "select 1 from users where email = ?";
    private static final String SELECT_ALL_STUDENTS = "select u.enabled u_enabled, u.id u_id, u.name u_name, u.email u_email," +
            "g.id g_id, g.name g_name from users u join `groups` g on u.group_id=g.id where u.role = 'STUDENT'";

    private static final String EXISTS_USER_BY_ID_AND_ENABLED = "select 1 from users where id = ? and enabled = 1";

    private static final String UPDATE_ENABLED = "update users set enabled = ? where id = ?";

    @Override
    public User getById(Connection connection, Long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_USER_BY_ID);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next() ? convertResultSetToUser(resultSet) : null;
    }

    @Override
    public List<User> getAll(Connection connection) throws SQLException {
        List<User> list = new ArrayList<>();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SELECT_ALL);
        while (resultSet.next()) {
            list.add(convertResultSetToUser(resultSet));
        }
        return list;
    }

    @Override
    public User getUserByEmailAndPassword(Connection connection, String email, String password) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_USER_BY_EMAIL_AND_PASSWORD);
        statement.setString(1, email);
        statement.setString(2, password);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next() ? convertResultSetToUser(resultSet) : null;
    }

    @Override
    public Long save(Connection connection, User user) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(INSERT_USER, PreparedStatement.RETURN_GENERATED_KEYS);
        setAttributeForSave(user, statement);
        statement.executeUpdate();
        return getGeneratedId(statement);

    }

    @Override
    public void update(Connection connection, User user) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE_USER);
        setAttributeForUpdate(user, statement);
        statement.executeUpdate();
    }

    @Override
    public void delete(Connection connection, Long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE_USER);
        statement.setLong(1, id);
        statement.executeUpdate();
    }

    @Override
    public boolean existsByIdAndEmail(Connection connection, Long id, String email) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(EXISTS_USER_BY_ID_AND_EMAIL);
        statement.setLong(1, id);
        statement.setString(2, email);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next();
    }

    @Override
    public boolean existsByEmail(Connection connection, String email) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(EXISTS_USER_BY_EMAIL);
        statement.setString(1, email);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next();
    }

    @Override
    public List<Student> getAllStudents(Connection connection) throws SQLException {
        List<Student> list = new ArrayList<>();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SELECT_ALL_STUDENTS);
        while (resultSet.next()) {
            list.add(convertResultSetToStudent(resultSet));
        }
        return list;
    }

    @Override
    public boolean isEnabledById(Connection connection, Long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(EXISTS_USER_BY_ID_AND_ENABLED);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next();
    }

    @Override
    public void updateEnabled(Connection connection, Long id, boolean b) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE_ENABLED);
        statement.setBoolean(1, b);
        statement.setLong(2, id);
        statement.executeUpdate();
    }

    private User convertResultSetToUser(ResultSet resultSet) throws SQLException {
        Student student = new Student();
        student.setId(resultSet.getLong("id"));
        student.setRole(Role.valueOf(resultSet.getString("role")));
        student.setName(resultSet.getString("name"));
        student.setEnabled(resultSet.getBoolean("enabled"));
        student.setPassword(resultSet.getString("password"));
        student.setEmail(resultSet.getString("email"));
        student.setGroup(new Entity((resultSet.getString("groups.name"))));
        return student;
    }

    private Student convertResultSetToStudent(ResultSet resultSet) throws SQLException {
        Student student = new Student();
        student.setId(resultSet.getLong("u_id"));
        student.setName(resultSet.getString("u_name"));
        student.setEnabled(resultSet.getBoolean("u_enabled"));
        student.setEmail(resultSet.getString("u_email"));
        student.setGroup(new Entity((resultSet.getString("g_name"))));
        student.getGroup().setId(resultSet.getLong("g_id"));
        return student;
    }

    private Long getGeneratedId(PreparedStatement statement) throws SQLException {
        ResultSet generatedKeys = statement.getGeneratedKeys();
        generatedKeys.next();
        return generatedKeys.getLong(1);
    }

    private void setAttributeForSave(User user, PreparedStatement statement) throws SQLException {
        int count = 1;
        statement.setString(count++, user.getName());
        statement.setString(count++, user.getPassword());
        statement.setString(count++, user.getEmail());
        statement.setString(count++, user.getRole().name());
        Long groupId = null;
        if (user instanceof Student) {
            groupId = ((Student) user).getGroup().getId();
        }
        statement.setObject(count, groupId);
    }

    private void setAttributeForUpdate(User user, PreparedStatement statement) throws SQLException {
        int count = 1;
        statement.setString(count++, user.getName());
        statement.setString(count++, user.getPassword());
        statement.setString(count, user.getEmail());
    }
}
