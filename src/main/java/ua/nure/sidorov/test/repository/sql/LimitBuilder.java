package ua.nure.sidorov.test.repository.sql;

public interface LimitBuilder extends PreparedStatementBuilder {

    PreparedStatementBuilder limit(int offset, int size);

}
