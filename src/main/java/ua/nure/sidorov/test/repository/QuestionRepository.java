package ua.nure.sidorov.test.repository;

import ua.nure.sidorov.test.entity.Question;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface QuestionRepository {

    Question getById(Connection connection, Long id) throws SQLException;

    List<Question> getAll(Connection connection, Long testId) throws SQLException;

    Long save(Connection connection, Question question) throws SQLException;

    void delete(Connection connection, Long id) throws SQLException;

    void update(Connection connection, Question question) throws SQLException;

    List<Question> getByTestId(Connection connection, Long testId) throws SQLException;
}
