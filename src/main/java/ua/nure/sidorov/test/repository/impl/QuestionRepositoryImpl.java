package ua.nure.sidorov.test.repository.impl;

import ua.nure.sidorov.test.entity.Question;
import ua.nure.sidorov.test.repository.QuestionRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class QuestionRepositoryImpl implements QuestionRepository {

    private static final String SELECT_QUESTION_BY_ID = "select * from questions where id = ?";
    private static final String SELECT_QUESTION_BY_TEST_ID = "select * from questions where test_id = ?";
    private static final String SELECT_ALL = "select * from questions";
    private static final String INSERT_QUESTION = "insert into questions (name, description, test_id) values (?,?,?)";
    private static final String DELETE_QUESTION = "delete from questions where id=?";
    private static final String UPDATE_QUESTION = "update questions set name=?, description=? where id = ?";
    private static final String DELETE_ALL_ANSWERS_BY_QUESTION_ID = "DELETE FROM answers WHERE question_id = ?";

    @Override
    public Question getById(Connection connection, Long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_QUESTION_BY_ID);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next() ? convertResultSetToQuestion(resultSet) : null;
    }

    @Override
    public List<Question> getAll(Connection connection, Long testId) throws SQLException {
        List<Question> list = new ArrayList<>();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SELECT_ALL);
        while (resultSet.next()) {
            list.add(convertResultSetToQuestion(resultSet));
        }
        return list;
    }

    @Override
    public Long save(Connection connection, Question question) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(INSERT_QUESTION, PreparedStatement.RETURN_GENERATED_KEYS);
        setAttributeForSave(question, statement);
        statement.executeUpdate();
        return getGeneratedId(statement);
    }

    @Override
    public void delete(Connection connection, Long id) throws SQLException {
        deleteAllAnswersByQuestionId(connection, id);
        PreparedStatement statement = connection.prepareStatement(DELETE_QUESTION);
        statement.setLong(1, id);
        statement.executeUpdate();
    }


    @Override
    public void update(Connection connection, Question question) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE_QUESTION);
        setAttributeForUpdate(question, statement);
        statement.executeUpdate();
    }

    @Override
    public List<Question> getByTestId(Connection connection, Long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_QUESTION_BY_TEST_ID);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();
        List<Question> questions = new ArrayList<>();
        while (resultSet.next()) {
            questions.add(convertResultSetToQuestion(resultSet));
        }
        return questions;
    }

    private void deleteAllAnswersByQuestionId(Connection connection, Long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE_ALL_ANSWERS_BY_QUESTION_ID);
        statement.setLong(1, id);
        statement.executeUpdate();
    }

    private void setAttributeForSave(Question question, PreparedStatement statement) throws SQLException {
        int count = 1;
        statement.setString(count++, question.getName());
        statement.setString(count++, question.getDescription());
        statement.setLong(count, question.getTest().getId());
    }

    private void setAttributeForUpdate(Question question, PreparedStatement statement) throws SQLException {
        int count = 1;
        statement.setString(count++, question.getName());
        statement.setString(count++, question.getDescription());
        statement.setLong(count, question.getId());
    }

    private Long getGeneratedId(PreparedStatement statement) throws SQLException {
        ResultSet generatedKeys = statement.getGeneratedKeys();
        generatedKeys.next();
        return generatedKeys.getLong(1);
    }

    private Question convertResultSetToQuestion(ResultSet resultSet) throws SQLException {
        Question question = new Question();
        question.setId(resultSet.getLong("id"));
        question.setName(resultSet.getString("name"));
        question.setDescription(resultSet.getString("description"));
        return question;
    }
}
