package ua.nure.sidorov.test.repository;

import ua.nure.sidorov.test.entity.Test;
import ua.nure.sidorov.test.entity.UserTest;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface TestRepository {

    Test getById(Connection connection, Long id) throws SQLException;

    List<Test> getAll(Connection connection, String subjectName, String orderBy, int offset, int size) throws SQLException;

    Long getCount(Connection connection, String subjectName) throws SQLException;

    Long save(Connection connection, Test Test) throws SQLException;

    void delete(Connection connection, Long id) throws SQLException;

    void update(Connection connection, Test Test) throws SQLException;

    void incrementTestQuestionSize(Connection connection, Long id) throws SQLException;

    void decrementTestQuestionSize(Connection connection, Long id) throws SQLException;

    List<UserTest> getTestResultsByUserId(Connection connection, Long id) throws SQLException;

    Long saveTestResultByUserIdAndTestId(Connection connection, UserTest userTest) throws SQLException;

    boolean getUsersTestsByTestId(Connection connection, Long id) throws SQLException;

    Long selectUserTestByUserAndTestId(Connection connection, Long userId, Long testId) throws SQLException;

    void updateUserTestById(Connection connection, UserTest userTest) throws SQLException;

    List<UserTest> getAllUsersTests(Connection c) throws SQLException;

}
