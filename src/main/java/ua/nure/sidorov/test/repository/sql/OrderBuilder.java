package ua.nure.sidorov.test.repository.sql;


public interface OrderBuilder extends LimitBuilder, PreparedStatementBuilder {

    LimitBuilder order(String fieldName, boolean ascending);

}
