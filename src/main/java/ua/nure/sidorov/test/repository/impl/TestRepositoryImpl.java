package ua.nure.sidorov.test.repository.impl;

import ua.nure.sidorov.test.entity.*;
import ua.nure.sidorov.test.repository.TestRepository;
import ua.nure.sidorov.test.repository.sql.SelectBuilder;
import ua.nure.sidorov.test.repository.sql.SqlBuilder;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TestRepositoryImpl implements TestRepository {

    private static final String DEFAULT_ORDER = "t.name,ASC";

    private static final String SELECT_TEST_JOIN_SUBJECT = "SELECT " +
            "t.id t_id, t.name t_name, t.complexity t_complexity, t.duration_minutes t_duration_minutes, " +
            "t.questions_size t_questions_size, " +
            "s.id s_id, s.name s_name " +
            "FROM tests t LEFT JOIN subjects s ON s.id=t.subject_id";

    private static final String COUNT_TEST = "SELECT COUNT(*) FROM tests t " +
            "LEFT JOIN subjects s ON s.id=t.subject_id";

    private static final String SELECT_TEST_BY_ID = SELECT_TEST_JOIN_SUBJECT + " where t.id = ?";

    private static final String SELECT_USERS_TESTS_BY_TEST_ID = "select 1 from users_tests where test_id = ?";

    private static final String INSERT_TEST = "INSERT INTO tests (name,complexity,duration_minutes,subject_id) " +
            "VALUES (?,?,?,?)";

    private static final String INSERT_TEST_RESULT = "insert into users_tests (user_id,test_id,mark) values (?,?,?)";

    private static final String UPDATE_TEST = "UPDATE tests SET name=?, complexity=?, duration_minutes=?, " +
            "subject_id=? WHERE id=?";

    private static final String UPDATE_USERS_TESTS = "UPDATE users_tests SET user_id = ?, test_id = ?, mark = ? WHERE id = ?";

    private static final String UPDATE_TEST_INCREMENT_QUESTION_SIZE = "UPDATE tests SET questions_size=questions_size+1 WHERE id=?";

    private static final String UPDATE_TEST_DECREMENT_QUESTION_SIZE = "UPDATE tests SET questions_size=questions_size-1 WHERE id=?";

    private static final String DELETE_TEST = "DELETE FROM tests WHERE id=?";

    private static final String SELECT_TEST_BY_USER_ID = "select t.name t_name, t.id t_id, s.name s_name, ut.mark ut_mark " +
            "from users_tests ut " +
            "join tests t on t.id=ut.test_id " +
            "join subjects s on s.id=t.subject_id " +
            "where ut.user_id=?";

    private static final String DELETE_ALL_ANSWERS_BY_TEST_ID = "DELETE FROM answers WHERE question_id in " +
            "(select q.id from questions q where q.test_id = ?)";

    private static final String DELETE_ALL_QUESTIONS_BY_TEST_ID = "DELETE FROM questions where test_id = ?";

    private static final String SELECT_USERS_TESTS_BY_USER_AND_TEST_ID = "SELECT * FROM users_tests where " +
            "user_id = ? and test_id = ?";

    private static final String SELECT_ALL_USERS_TESTS = "select t.name t_name, " +
            "s.name s_name, " +
            "u.name u_name, u.email u_email, " +
            "g.name g_name, " +
            "ut.mark ut_mark " +
            "from users_tests ut " +
            "join users u on u.id = ut.user_id " +
            "join `groups` g on g.id = u.group_id " +
            "join tests t on t.id=ut.test_id " +
            "join subjects s on s.id=t.subject_id " +
            "where u.role = 'STUDENT'";

    private static final String SELECT_AVG_TEST_MARK_BY_SUBJECT_ID = "select t.id t_id, " +
            "t.name t_name, " +
            "t.complexity t_complexity, " +
            "t.duration_minutes t_duration_minutes, " +
            "t.questions_size t_questions_size, " +
            "s.id s_id, s.name s_name, " +
            "(select avg(ut.mark) " +
                "from users_tests ut join tests sub_t on sub_t.id=ut.test_id " +
                "join subjects sub_s on sub_s.id=sub_t.subject_id " +
                "where sub_s.id = s.id) average_mark " +
            "FROM tests t LEFT JOIN subjects s ON s.id=t.subject_id";

    @Override
    public Test getById(Connection connection, Long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_TEST_BY_ID);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next() ? convertResultSetToTest(resultSet) : null;
    }

    @Override
    public List<UserTest> getTestResultsByUserId(Connection connection, Long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_TEST_BY_USER_ID);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();

        List<UserTest> userTests = new ArrayList<>();
        while (resultSet.next()) {
            userTests.add(convertResultSetToTestHistory(resultSet));
        }
        return userTests;
    }

    @Override
    public List<Test> getAll(Connection connection, String subjectName, String orderBy, int offset, int size) throws SQLException {
        SelectBuilder select = SqlBuilder.select(SELECT_AVG_TEST_MARK_BY_SUBJECT_ID);
        setFilter(subjectName, select);
        setOrder(orderBy, select);
        select.limit(offset, size);
        PreparedStatement statement = select.buildPrepareStatement(connection);
        ResultSet resultSet = statement.executeQuery();

        List<Test> tests = new ArrayList<>();
        while (resultSet.next()) {
            tests.add(convertResultSetToTestWithAverage(resultSet));
        }
        return tests;
    }

    @Override
    public Long getCount(Connection connection, String subjectName) throws SQLException {
        SelectBuilder select = SqlBuilder.select(COUNT_TEST);
        setFilter(subjectName, select);
        PreparedStatement statement = select.buildPrepareStatement(connection);
        ResultSet resultSet = statement.executeQuery();

        return resultSet.next() ? resultSet.getLong(1) : 0L;
    }

    @Override
    public Long save(Connection connection, Test test) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(INSERT_TEST, PreparedStatement.RETURN_GENERATED_KEYS);
        setAttributeForSave(test, statement);
        statement.executeUpdate();
        return getGeneratedId(statement);
    }

    @Override
    public Long saveTestResultByUserIdAndTestId(Connection connection, UserTest userTest) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(INSERT_TEST_RESULT, PreparedStatement.RETURN_GENERATED_KEYS);
        setAttributeForSaveResultTest(statement, userTest);
        statement.executeUpdate();
        return getGeneratedId(statement);
    }

    @Override
    public void delete(Connection connection, Long id) throws SQLException {
        deleteAllAnswersByTestId(connection, id);
        deleteAllQuestionsByTestId(connection, id);
        PreparedStatement statement = connection.prepareStatement(DELETE_TEST);
        statement.setLong(1, id);
        statement.executeUpdate();
    }

    @Override
    public List<UserTest> getAllUsersTests(Connection connection) throws SQLException {
        List<UserTest> list = new ArrayList<>();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(SELECT_ALL_USERS_TESTS);
        while (resultSet.next()) {
            list.add(convertResultSetToUserTest(resultSet));
        }
        return list;
    }

    @Override
    public boolean getUsersTestsByTestId(Connection connection, Long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_USERS_TESTS_BY_TEST_ID);
        statement.setLong(1, id);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next();
    }

    @Override
    public Long selectUserTestByUserAndTestId(Connection connection, Long userId, Long testId) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_USERS_TESTS_BY_USER_AND_TEST_ID);
        statement.setLong(1, userId);
        statement.setLong(2, testId);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next() ? resultSet.getLong("id") : null;
    }

    @Override
    public void update(Connection connection, Test test) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE_TEST);
        setAttributeForUpdate(test, statement);
        statement.executeUpdate();
    }

    @Override
    public void updateUserTestById(Connection connection, UserTest userTest) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE_USERS_TESTS);
        setAttributeForUpdateUserTest(userTest, statement);
        statement.executeUpdate();
    }

    @Override
    public void incrementTestQuestionSize(Connection connection, Long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE_TEST_INCREMENT_QUESTION_SIZE);
        statement.setLong(1, id);
        statement.executeUpdate();
    }

    @Override
    public void decrementTestQuestionSize(Connection connection, Long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(UPDATE_TEST_DECREMENT_QUESTION_SIZE);
        statement.setLong(1, id);
        statement.executeUpdate();
    }

    private void deleteAllAnswersByTestId(Connection connection, Long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE_ALL_ANSWERS_BY_TEST_ID);
        statement.setLong(1, id);
        statement.executeUpdate();
    }

    private void deleteAllQuestionsByTestId(Connection connection, Long id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE_ALL_QUESTIONS_BY_TEST_ID);
        statement.setLong(1, id);
        statement.executeUpdate();
    }

    private void setAttributeForSave(Test test, PreparedStatement statement) throws SQLException {
        int count = 0;
        statement.setString(++count, test.getName());
        statement.setString(++count, test.getComplexity().name());
        statement.setInt(++count, test.getDurationMinutes());
        statement.setLong(++count, test.getSubject().getId());
    }

    private void setAttributeForSaveResultTest(PreparedStatement statement, UserTest userTest) throws SQLException {
        int count = 1;
        statement.setLong(count++, userTest.getUser().getId());
        statement.setLong(count++, userTest.getTest().getId());
        statement.setDouble(count, userTest.getMark());
    }

    private void setAttributeForUpdate(Test test, PreparedStatement statement) throws SQLException {
        int count = 0;
        statement.setString(++count, test.getName());
        statement.setString(++count, test.getComplexity().name());
        statement.setInt(++count, test.getDurationMinutes());
        statement.setLong(++count, test.getSubject().getId());
        statement.setLong(++count, test.getId());
    }

    private void setAttributeForUpdateUserTest(UserTest userTest, PreparedStatement statement) throws SQLException {
        int count = 0;
        statement.setLong(++count, userTest.getUser().getId());
        statement.setLong(++count, userTest.getTest().getId());
        statement.setDouble(++count, userTest.getMark());
        statement.setLong(++count, userTest.getId());
    }

    private Test convertResultSetToTest(ResultSet resultSet) throws SQLException {
        Test test = new Test();
        test.setId(resultSet.getLong("t_id"));
        test.setName(resultSet.getString("t_name"));
        test.setComplexity(Complexity.valueOf(resultSet.getString("t_complexity")));
        test.setDurationMinutes(resultSet.getInt("t_duration_minutes"));
        Entity subject = new Entity();
        subject.setId(resultSet.getLong("s_id"));
        subject.setName(resultSet.getString("s_name"));
        test.setSubject(subject);
        test.setQuestionsSize(resultSet.getInt("t_questions_size"));
        return test;
    }
    
    private Test convertResultSetToTestWithAverage(ResultSet resultSet) throws SQLException {
        Test test = new Test();
        test.setId(resultSet.getLong("t_id"));
        test.setName(resultSet.getString("t_name"));
        test.setComplexity(Complexity.valueOf(resultSet.getString("t_complexity")));
        test.setDurationMinutes(resultSet.getInt("t_duration_minutes"));
        Entity subject = new Entity();
        subject.setId(resultSet.getLong("s_id"));
        subject.setName(resultSet.getString("s_name")+String.format("(%.1f)", resultSet.getDouble("average_mark")*100));
        test.setSubject(subject);
        test.setQuestionsSize(resultSet.getInt("t_questions_size"));
        return test;
    }

    private UserTest convertResultSetToTestHistory(ResultSet resultSet) throws SQLException {
        UserTest userTest = new UserTest();
        userTest.setTest(new Test());
        userTest.getTest().setId(resultSet.getLong("t_id"));
        userTest.getTest().setName(resultSet.getString("t_name"));
        userTest.getTest().setSubject(new Entity());
        userTest.getTest().getSubject().setName(resultSet.getString("s_name"));
        userTest.setMark(resultSet.getDouble("ut_mark"));
        return userTest;
    }

    private UserTest convertResultSetToUserTest(ResultSet resultSet) throws SQLException {
        UserTest userTest = new UserTest();
        Test test = new Test();
        test.setName(resultSet.getString("t_name"));
        test.setSubject(new Entity());
        test.getSubject().setName(resultSet.getString("s_name"));
        Student user = new Student();
        user.setName(resultSet.getString("u_name"));
        user.setEmail(resultSet.getString("u_email"));
        user.setGroup(new Entity());
        user.getGroup().setName(resultSet.getString("g_name"));
        userTest.setTest(test);
        userTest.setUser(user);
        userTest.setMark(resultSet.getDouble("ut_mark"));
        return userTest;
    }

    private Long getGeneratedId(PreparedStatement statement) throws SQLException {
        ResultSet generatedKeys = statement.getGeneratedKeys();
        generatedKeys.next();
        return generatedKeys.getLong(1);
    }

    private void setOrder(String orderBy, SelectBuilder select) {
        if (Objects.isNull(orderBy)) {
            orderBy = DEFAULT_ORDER;
        }
        String field = orderBy.split(",")[0];
        boolean ascending = !orderBy.split(",")[1].equals("DESC");
        select.order(field, ascending);
    }

    private void setFilter(String subjectName, SelectBuilder select) {
        if (Objects.nonNull(subjectName)) {
            select.where().like("s.name", subjectName);
        }
    }

}