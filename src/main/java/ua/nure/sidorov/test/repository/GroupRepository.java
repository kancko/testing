package ua.nure.sidorov.test.repository;

import ua.nure.sidorov.test.entity.Entity;

import java.sql.Connection;
import java.sql.SQLException;

public interface GroupRepository {
    Entity getByName(Connection connection, String name) throws SQLException;

    boolean existByName(Connection connection, String name) throws SQLException;

    Long save(Connection connection, Entity group) throws SQLException;
}
