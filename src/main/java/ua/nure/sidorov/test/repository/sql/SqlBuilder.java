package ua.nure.sidorov.test.repository.sql;

import ua.nure.sidorov.test.repository.sql.impl.SelectBuilderImpl;

public class SqlBuilder {

    public static SelectBuilder select(String selectQuery){
        return new SelectBuilderImpl(selectQuery);
    }

}
