package ua.nure.sidorov.test.repository.impl;

import ua.nure.sidorov.test.entity.Entity;
import ua.nure.sidorov.test.repository.SubjectRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SubjectRepositoryImpl implements SubjectRepository {


    private static final String SELECT_SUBJECT_BY_NAME = "select * from subjects where name = ?";
    private static final String EXIST_SUBJECT_BY_NAME = "select 1 from subjects where name = ?";
    private static final String INSERT_SUBJECT = "insert into subjects (name) values (?)";

    @Override
    public Entity getByName(Connection connection, String name) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_SUBJECT_BY_NAME);
        statement.setString(1, name);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next() ? convertResultSetToSubject(resultSet) : null;
    }

    @Override
    public boolean existByName(Connection connection, String name) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(EXIST_SUBJECT_BY_NAME);
        statement.setString(1, name);
        ResultSet resultSet = statement.executeQuery();
        return resultSet.next();
    }

    @Override
    public Long save(Connection connection, Entity subject) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(INSERT_SUBJECT, PreparedStatement.RETURN_GENERATED_KEYS);
        setAttributeForSave(subject, statement);
        statement.executeUpdate();
        return getGeneratedId(statement);
    }

    private void setAttributeForSave(Entity subject, PreparedStatement statement) throws SQLException {
        statement.setString(1, subject.getName());
    }

    private Long getGeneratedId(PreparedStatement statement) throws SQLException {
        ResultSet generatedKeys = statement.getGeneratedKeys();
        generatedKeys.next();
        return generatedKeys.getLong(1);
    }

    private Entity convertResultSetToSubject(ResultSet resultSet) throws SQLException {
        Entity subject = new Entity();
        subject.setId(resultSet.getLong("id"));
        subject.setName(resultSet.getString("name"));
        return subject;
    }
    
}
