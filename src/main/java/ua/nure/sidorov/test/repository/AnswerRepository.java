package ua.nure.sidorov.test.repository;

import ua.nure.sidorov.test.entity.Answer;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface AnswerRepository {

    List<Answer> getAll(Connection connection) throws SQLException;

    Long save(Connection connection, Answer user) throws SQLException;

    void delete(Connection connection, Long id) throws SQLException;

    void update(Connection connection, Answer user) throws SQLException;

    List<Answer> getByQuestionId(Connection connection, Long id) throws SQLException;

    List<Answer> getCorrectByQuestionId(Connection connection, Long id) throws SQLException;
}
