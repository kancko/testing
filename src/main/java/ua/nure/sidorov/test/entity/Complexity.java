package ua.nure.sidorov.test.entity;

public enum Complexity {
    LOW, MEDIUM, HARD
}
