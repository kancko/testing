package ua.nure.sidorov.test.entity;

import java.util.List;

public class Test extends Entity {
    private Entity subject;
    private Complexity complexity;
    private Integer durationMinutes;
    private List<Question> questions;
    private Integer questionsSize;

    public Test() {

    }

    public Test(Long id) {
        setId(id);
    }

    public Entity getSubject() {
        return subject;
    }

    public void setSubject(Entity subject) {
        this.subject = subject;
    }

    public Complexity getComplexity() {
        return complexity;
    }

    public void setComplexity(Complexity complexity) {
        this.complexity = complexity;
    }

    public Integer getDurationMinutes() {
        return durationMinutes;
    }

    public void setDurationMinutes(Integer durationMinutes) {
        this.durationMinutes = durationMinutes;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public Integer getQuestionsSize() {
        return questionsSize;
    }

    public void setQuestionsSize(Integer questionsSize) {
        this.questionsSize = questionsSize;
    }
}
