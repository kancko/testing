package ua.nure.sidorov.test.entity;

import java.util.List;

public class Question extends Entity {
    private Test test;
    private String description;
    private List<Answer> answers;

    public Question() {

    }

    public Question(Long id) {
        setId(id);
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }
}
