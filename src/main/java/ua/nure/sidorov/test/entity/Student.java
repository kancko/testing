package ua.nure.sidorov.test.entity;

import java.util.List;

public class Student extends User {
    private Entity group;
    private List<Test> tests;

    public Entity getGroup() {
        return group;
    }

    public void setGroup(Entity group) {
        this.group = group;
    }

    public List<Test> getTests() {
        return tests;
    }

    public void setTests(List<Test> tests) {
        this.tests = tests;
    }

}
