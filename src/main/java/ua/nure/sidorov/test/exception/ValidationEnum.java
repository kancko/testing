package ua.nure.sidorov.test.exception;

public enum ValidationEnum {
    BAD_CREDENTIAL,
    DELETE_ERROR,
    EMAIL_EXISTS,
    PASSWORDS_DO_NOT_MATCH,
    NUMBER_OF_ANSWERS_IS_NULL
}
