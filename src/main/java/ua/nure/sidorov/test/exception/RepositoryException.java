package ua.nure.sidorov.test.exception;

public class RepositoryException extends RuntimeException{

    public RepositoryException(Throwable cause) {
        super(cause);
    }
}
