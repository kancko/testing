package ua.nure.sidorov.test.transaction.impl;

import ua.nure.sidorov.test.exception.RepositoryException;
import ua.nure.sidorov.test.transaction.TransactionAction;
import ua.nure.sidorov.test.transaction.TransactionManager;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class TransactionManagerImpl implements TransactionManager {

    private DataSource dataSource;

    public TransactionManagerImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public <U> U execute(TransactionAction<U> transactionAction) {
        U result;
        try (Connection connection = dataSource.getConnection()) {
            connection.setAutoCommit(false);
            result = transactionAction.action(connection);
            connection.commit();
        } catch (SQLException ex) {
            throw new RepositoryException(ex);
        }
        return result;
    }
}
