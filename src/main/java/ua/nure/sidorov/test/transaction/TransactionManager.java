package ua.nure.sidorov.test.transaction;

public interface TransactionManager {

    <U> U execute(TransactionAction<U> transactionAction);

}
